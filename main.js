function hitungVolumeLimas(alas, tinggiLimas) {
  // Hitung luas alas segitiga
  const luasAlas = 0.5 * alas * tinggiLimas;

  // Hitung volume limas
  const volumeLimas = (1 / 3) * luasAlas * tinggiLimas;

  return volumeLimas;
}
console.log(hitungVolumeLimas(10, 8));

function hitungVolumeKerucut(jari, tinggi) {
  // Hitung Luas Alas
  const luasAlas = Math.PI * jari ** 2;

  // Hitung Luas Kerucut
  const volumeKerucut = (1 / 3) * luasAlas * tinggi;

  return volumeKerucut;
}

console.log(hitungVolumeKerucut(7, 10));

function hitungVolumePrisma(alas, tinggiPrisma, tinggiSegitiga) {
  // Hitung luas alas segitiga
  const luasAlas = 0.5 * alas * tinggiSegitiga;

  // Hitung volume prisma
  const volumePrisma = luasAlas * tinggiPrisma;

  return volumePrisma;
}
console.log(hitungVolumePrisma(10, 8, 6));

function hitungVolumeTabung(jariJari, tinggiTabung) {
  // Hitung volume tabung
  const volumeTabung = Math.PI * Math.pow(jariJari, 2) * tinggiTabung;

  return volumeTabung;
}
console.log(hitungVolumeTabung(5, 10));

function volumeBola(jari2) {
  const volumeBola = (3 / 4) * 3.14 * jari2 ** 3;
  return volumeBola;
}
console.log(volumeBola(7));

console.log("hello world")
